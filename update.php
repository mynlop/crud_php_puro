<?php
    require_once 'config.php';

    $result = false;

    if(!empty($_POST)){
        $id = $_POST['id'];
        $newName = $_POST['name'];
        $newEmail = $_POST['email'];

        $sql = "UPDATE users SET name=:name, email=:email WHERE id=:id";
        $query = $pdo->prepare($sql);

        $result = $query->execute([
            'id' => $id,
            'name' => $newName,
            'email' => $newEmail
        ]);
        $name = $newName;
        $email = $newEmail;
    }else{
        $id = $_GET['id'];
        
        $sql = "SELECT * FROM users WHERE id=:id";

        $query = $pdo->prepare($sql);
        $query->execute([
            'id' => $id
        ]);
        $row = $query->fetch(PDO::FETCH_ASSOC);
        $name = $row['name'];
        $email = $row['email'];
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
</head>
<body>
    <div class="container">
        <h1>Update Users</h1>
        <a href="index.php">Home</a>

        <?php
            if($result){
                echo '<div class="alert alert-success">Correcta la actualización.</div>';
            }
        ?>

        <form action="update.php" method="POST" class="form-inline">
            <div class="form-group">
                <label for="txtName" class="control-label">Name</label>
                <input class="form-control" type="text" name="name" id="txtName" value=<?php echo $name; ?> />
            </div>
            <br>
            <div class="form-group">
                <label for="txtEmail">Email</label>
                <input class="form-control" type="email" name="email" id="txtEmail" value=<?php echo $email; ?> />
            </div>
            <br>
            <input type="hidden" name="id" value=<?php echo $id; ?> />
            <input class="btn btn-primary" type="submit" value="Update">
        </form>
    </div>
</body>
</html>