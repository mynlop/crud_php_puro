<?php
    $user = null;
    $query = null;

    if(!empty($_POST)){
        require_once 'config.php';

        // esta forma no es segura
        // $query = "SELECT * FROM users WHERE email = '" .$_POST['email'] . "' AND password = '" . md5($_POST['pass']) ."'";
        $query = "SELECT * FROM users WHERE email = :email AND password = :password";
        // $queryResult = $pdo->query($query);
        $prepared = $pdo->prepare($query);
        $prepared->execute([
            'email' => $_POST['email'],
            'password' => md5($_POST['pass'])
        ]);
        $user = $prepared->fetch(PDO::FETCH_ASSOC);
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
</head>
<body>
    <div class="container">
        <h1>Fake Login</h1>
        <a href="index.php">Home</a>
        <form action="fake-login.php" method="POST" class="form-inline">
            <div class="form-group">
                <label for="txtEmail">Email</label>
                <input type="email" name="email" id="txtEmail" class="form-control">
            </div>
            <div class="form-group">
                <label for="txtPass">Password</label>
                <input type="password" name="pass" id="txtPass" class="form-control">
            </div>
            <input type="submit" value="Login" class="btn btn-primary">
        </form>

        <div class="container">
            <h2>Query</h2>
            <?php
                print_r($query);
            ?>
        </div>
        <div class="container">
            <h2>User Data</h2>
            <?php
                print_r($user);
            ?>
        </div>
    </div>
</body>
</html>