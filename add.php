<?php
    require_once 'config.php';

    $result = false;

    if(!empty($_POST)){
        $name = $_POST['name'];
        $email = $_POST['email'];
        $pass = md5($_POST['pass']);

        $sql = "INSERT INTO users(name, email, password) VALUES(:name, :email, :pass)";
        $query = $pdo->prepare($sql);
        $result = $query->execute([
            'name' => $name,
            'email' => $email,
            'pass' => $pass
        ]);
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDO_PHP |ADD|</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
</head>
<body>
    <div class="container">
        <h1>Add User</h1>
        <a href="index.php">Home</a>

        <?php 
            if($result){
                echo '<div class="alert alert-success">Correcto!</div>';
            }
        ?>
        <form action="add.php" method="post" class="form-inline">
            <div class="form-group">
                <label for="txtName">Name:</label>
                <input class="form-control" type="text" name="name" id="txtName">
            </div>
            <br>
            <div class="form-group">
                <label for="txtEmail">Email:</label>
                <input class="form-control" type="email" name="email" id="txtEmail">
            </div>
            <br>
            <div class="form-group">
                <label for="txtPass">Password:</label>
                <input class="form-control" type="password" name="pass" id="txtPass">
            </div>
            <br>
            <input class="btn btn-primary" type="submit" value="Save">
        </form>
    </div>
</body>
</html>